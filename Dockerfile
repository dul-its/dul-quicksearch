FROM image-mirror-prod-registry.cloud.duke.edu/library/ruby:3.2.0

USER 0

RUN apt-get update -qq \
  && apt-get install -y --no-install-recommends \
  nodejs \
  npm \
  shared-mime-info \
  wait-for-it \
  && rm -rf /var/lib/apt/lists/*

RUN mkdir /usr/src/app

# Create non-root group and users
RUN groupadd -g 1001 quicksearch && useradd -u 1001 -g 1001 quicksearch

ENV GEM_HOME="/usr/local/bundle"
ENV PATH $GEM_HOME/bin:$GEM_HOME/gems/bin:$PATH

RUN bundle config --global frozen 1
WORKDIR /usr/src/app
COPY Gemfile* ./
RUN gem install bundler -v 2.5.6
RUN bundle install

COPY . .

RUN npm install -g yarn
RUN yarn install
RUN bundle exec rake assets:precompile

COPY entrypoint.sh /usr/local/bin
RUN chown -R 1001:1001 /usr/src/app && chmod -R a+rw /usr/src/app
RUN chmod +x /usr/local/bin/*
RUN mkdir /.cache
RUN chmod -R a+rw /.cache

USER 1001

ENTRYPOINT ["entrypoint.sh"]
CMD ["rails", "server", "-b", "0.0.0.0", "-p", "3000"]
