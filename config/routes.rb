Rails.application.routes.draw do
  mount QuickSearch::Engine => "/"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  # exceptions
  get "/403", to: "errors#forbidden", via: :all
  get "/404", to: "errors#not_found", via: :all
  get "/422", to: "errors#rejected", via: :all
  get "/500", to: "errors#internal_server", via: :all
end
