# frozen_string_literal: true

# Monkey-patched extensions to QuickSearch core Appstats controller:
# https://github.com/NCSU-Libraries/quick_search/blob/master/app/controllers/quick_search/appstats_controller.rb

# Last checked for updates: QuickSearch v0.2.0

# QuickSearch::AppstatsController.class_eval do
  # Bugfix patch; QuickSearch core uses "<>" for NOT EQUAL comparison, which breaks
  # def excluded_categories
    # "category != 'common-searches' AND category != 'result-types' AND category != 'typeahead'"
  # end
# end
