# Quicksearch
[[_TOC_]]
## Preamble

## Prequisites

## Getting Starting (Developers)

## Deployments
Both the production and sandbox instances of the DUL Quicksearch application are 
deployed to OIT's OKD cluster.  
  
For each instance, a Gitlab "webhook" is configured to respond to commits to the `main` branch.
### Production
**OKD Console**  
https://manage.cloud.duke.edu/console/project/dul-dst/overview  
  

### Sandbox
**OKD Console**  
https://manage.cloud.duke.edu/console/project/dul-sandbox/overview  

#### Adjusting Enviroment Variables
For either instance, navigate to the appropriate console URL, then locate the most recent deployment.  
In this case:
* Look for the "dul-quicksearch" application section,
* Locate the "quicksearch-web" deployment
* Click the "quicksearch-web" link
* Click the "Enviroments" tab, then make any adjustments and save.

#### Start a Build (Manually)
Visit the OKD console (URL above), then:
1. Locate and click "Builds" in the left sidebar.
2. Click "Quicksearch" from the build list.
3. Locate (top-right) and click "Start Build"
  
A new deployment will rollout at the completion of the build.


## SOLR Integration
### Production
Set `WEBSITE_SEARCHER_BASE_URL=http://nutch-2.lib.duke.edu:8983/solr/nutch/select?`

### Sandbox
Set `WEBSITE_SEARCHER_BASE_URL=http://nutch-2.lib.duke.edu:8983/solr/nutch/select?`.
  
## Future Considerations/Plans
DevOps is planning to refactor the deployment strategy for both instances.  
  
#### New OKD Namesapces
New OKD namespaces will be created for the purposes of running the DUL Quicksearch 
applications.  
  
`dul-quicksearch-prod` will run the production instance.  
`dul-quicksearch-stage` will run the staging instance.  
  
Developers will continue to use their "local development environments" to run defacto 
"sandbox" instances - via Docker.  

#### Helm Chart
A helm chart will be created to facilitate the installation of DUL Quicksearch to 
the appropriate namespace (see above).

## Local Development / Docker
To pass summon secrets to the app, copy env-example to dev.env and update values for:
`SUMMON_ACCESS_ID`
`SUMMON_SECRET_KEY`
`SUMMON_CLIENT_BASE`
`LIBKEY_ID`
`LIBKEY_API_TOKEN`

[values available in OKD](https://manage.cloud.duke.edu/console/project/dul-dst/browse/secrets/api-secrets)


To build the app, run:
`docker-compose up --build`

To run rspec tests:
`docker-compose run -e "RAILS_ENV=test" web bundle exec rspec`

You may need to stop and remove the container if there are changes to the Gemfile:
`docker stop #{container_id}`
`docker rm #{container_id}`

## DevOps Stuff (Deprecated)
### OpenShift Template
You can find the deployment template at `openshift/templates/quicksearch.yaml`.  

This file includes:
* Secrets for database info (user, password) and `secret_key_base`
* DeploymentConfig specs the `quicksearch-web` and `postgresql` containers
* BuildConfig spec for building the `web` container from our Dockerfile
* Service specs for `quicksearch-web` and `postgresql`
* TODO Route definition for `quicksearch-web`
  
#### Notable Defaults
`APPLICATION_DOMAIN` defaults to **https://quicksearch-web-dul-dst.cloud.duke.edu**
