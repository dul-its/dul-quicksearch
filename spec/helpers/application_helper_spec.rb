# frozen_string_literal: true

describe ApplicationHelper do
  before do
    allow(helper).to receive(:action_name).and_return(action_name)
  end

  describe '#autosuggest_state' do
    context 'when single searcher' do
      let(:action_name) { 'single_searcher' }

      it 'is set to false' do
        expect(helper.autosuggest_state).to eq('false')
      end
    end

    context 'when default searcher' do
      let(:action_name) { 'default' }

      it 'is set to true' do
        expect(helper.autosuggest_state).to eq('true')
      end
    end
  end

  describe '#autocomplete_state' do
    context 'when single searcher' do
      let(:action_name) { 'single_searcher' }

      it 'is set to on' do
        expect(helper.autocomplete_state).to eq('on')
      end
    end

    context 'when default searcher' do
      let(:action_name) { 'default' }

      it 'is set to off' do
        expect(helper.autocomplete_state).to eq('off')
      end
    end
  end
end
