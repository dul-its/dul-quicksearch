# frozen_string_literal: true

describe QuickSearch::ApplicationHelper do
  before do
    allow(helper).to receive_messages(action_name:, params: { searcher_name: 'website' })
  end

  describe '#application_name' do
    context 'when single searcher' do
      let(:action_name) { 'single_searcher' }

      it 'is set to a custom title' do
        expect(helper.application_name).to eq('Website Search Results')
      end
    end

    context 'when default searcher' do
      let(:action_name) { 'default' }

      it 'is set to the default title' do
        expect(helper.application_name).to eq('Search Results')
      end
    end
  end
end
