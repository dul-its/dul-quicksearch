# frozen_string_literal: true

describe QuickSearch::CatalogSearcher do
  let(:searcher) do
    described_class.new(HTTPClient.new, 'how milton works', 1)
  end

  it 'has HTTPClient' do
    expect(searcher.http).to be_a(HTTPClient)
  end

  it 'has a query' do
    expect(searcher.q).to eq('how milton works')
  end

  it 'has per page' do
    expect(searcher.per_page).to eq(1)
  end

  it 'has a base url' do
    expect(searcher.base_url).to(
      eq('https://find.library.duke.edu/catalog.json?')
    )
  end

  it 'has parameters' do
    expect(searcher.parameters).to(
      eq({ q: 'how milton works', rows: 1, search_field: 'all_fields' })
    )
  end

  it 'has a loaded link' do
    expect(searcher.loaded_link).to(
      eq('https://find.library.duke.edu/?' \
         'search_field=all_fields&q=how+milton+works')
    )
  end

  describe '#results' do
    describe 'physical result' do
      before do
        VCR.use_cassette('searchers/catalog_searcher/milton') do
          searcher.search
        end
      end

      let(:results) do
        VCR.use_cassette('searchers/catalog_searcher/milton_syndetics') do
          searcher.results
        end
      end

      it 'has a total' do
        expect(searcher.total).to be_truthy
      end

      it 'has a title' do
        expect(results.first.title).to eq('How Milton works')
      end

      it 'has a link' do
        expect(results.first.link).to(
          eq('https://find.library.duke.edu/catalog/DUKE003007532')
        )
      end

      it 'has an author' do
        expect(results.first.author).to eq('Stanley Fish.')
      end

      it 'has a format' do
        expect(results.first.format).to eq('Book')
      end

      it 'has availability' do
        expect(results.first.availability).to eq('Available')
      end

      it 'has a location' do
        expect(results.first.location).to(
          eq('Perkins & Bostock Library, PR3588&nbsp;.F57&nbsp;2001&nbsp;c.1')
        )
      end

      it 'is not a full text result' do
        expect(results.first.fulltext).to be false
      end

      it 'does not have a full text link' do
        expect(results.first.fulltext_link).to be_nil
      end

      it 'has a thumbnail' do
        expect(results.first.thumbnail).to(
          # eq('https://syndetics.com/index.php?client=trlnet&isbn=0674004655%2FMC.GIF')
          eq('https://syndetics.com/index.php?client=trlnet&isbn=0674004655&oclc=45356608/MC.gif')
        )
      end
    end

    describe 'full text result' do
      let(:searcher) do
        described_class.new(HTTPClient.new, 'how parliament works milton', 1)
      end
      let(:results) do
        VCR.use_cassette('searchers/catalog_searcher/parliament_syndetics') do
          searcher.results
        end
      end

      before do
        VCR.use_cassette('searchers/catalog_searcher/parliament') do
          searcher.search
        end
      end

      it 'is a full text result' do
        expect(results.first.fulltext).to be true
      end

      it 'does not have a full text link' do
        expect(results.first.fulltext_link).to(
          eq('https://login.proxy.lib.duke.edu/login?url=' \
             'http://www.duke.eblib.com/EBLWeb/patron/?' \
             'target=patron&extendedid=P_2001979_0')
        )
      end
    end
  end

  describe 'location display' do
    # NOTE: See '#results: physical results: has a location' spec for single
    #       item display with call number.

    context 'when has multiple items but one location' do
      let(:searcher) do
        described_class.new(HTTPClient.new, '003778210', 1)
      end
      let(:result) do
        VCR.use_cassette('searchers/catalog_searcher/003778210_syndetics') do
          searcher.results
        end
      end

      before do
        VCR.use_cassette('searchers/catalog_searcher/003778210') do
          searcher.search
        end
      end

      it 'displays one broad location' do
        expect(result.first.location).to(
          eq('Perkins & Bostock Library')
        )
      end
    end

    context 'when has three locations' do
      let(:searcher) do
        described_class.new(HTTPClient.new, '000158580', 1)
      end
      let(:result) do
        VCR.use_cassette('searchers/catalog_searcher/000158580_syndetics') do
          searcher.results
        end
      end

      before do
        VCR.use_cassette('searchers/catalog_searcher/000158580') do
          searcher.search
        end
      end

      it 'displays all three broad locations without call numbers' do
        expect(result.first.location).to(
          eq('Library Service Center, Medical Center Library, Goodson Law Library')
        )
      end
    end

    context 'when has more than three locations' do
      let(:searcher) do
        described_class.new(HTTPClient.new, '000279773', 1)
      end
      let(:result) do
        VCR.use_cassette('searchers/catalog_searcher/000279773_syndetics') do
          searcher.results
        end
      end

      before do
        VCR.use_cassette('searchers/catalog_searcher/000279773') do
          searcher.search
        end
      end

      it 'displays the first three locations without call numbers' do
        expect(result.first.location).to(
          # eq('Perkins & Bostock Library, Goodson Law Library, Rubenstein Library...')
          eq('Goodson Law Library, Rubenstein Library, Library Service Center...')
        )
      end
    end
  end
end
