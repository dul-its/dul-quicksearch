# frozen_string_literal: true

describe QuickSearch::WebsiteSearcher do
  let(:searcher) do
    described_class.new(HTTPClient.new, 'Study Zone Policies', 1)
  end

  it 'has HTTPClient' do
    expect(searcher.http).to be_a(HTTPClient)
  end

  it 'has a query' do
    expect(searcher.q).to eq('Study Zone Policies')
  end

  it 'has per page' do
    expect(searcher.per_page).to eq(1)
  end

  it 'has a base url' do
    expect(searcher.base_url).to(
      eq('http://nutch-2.lib.duke.edu:8983/solr/nutch/select?')
    )
  end

  # rubocop:disable RSpec/ExampleLength
  it 'has parameters' do
    expect(searcher.parameters).to(
      eq({ bq: 'subcollection:main^100',
           defType: 'edismax',
           echoParams: 'explicit',
           hl: 'true',
           'hl.fl' => 'content',
           mm: '3<-1 5<-2 6<90%',
           pf: 'title^500 slug^100 anchor^100 h1^100 h2^50 h3^24 content^5',
           q: 'study zone policies',
           'q.alt' => '*:*',
           qf: 'title^250 slug^50 anchor^50 h1^50 h2^25 h3^12 content',
           rows: 1,
           start: 0 })
    )
  end
  # rubocop:enable RSpec/ExampleLength

  # NOTE: No loaded link available until standalone website search page is created.
  it 'has a loaded link' do
    expect(searcher.loaded_link).to(
      eq('/searcher/website?q=study+zone+policies')
    )
  end

  describe '#results' do
    before do
      VCR.use_cassette('searchers/website_searcher/study_zone') do
        searcher.search
      end
    end

    let(:results) do
      searcher.results
    end

    it 'has a total' do
      expect(searcher.total).to be_truthy
    end

    it 'has a title' do
      expect(results.first.title).to(
        eq('Study Zone Policies | Duke University Libraries')
      )
    end

    it 'has a link' do
      expect(results.first.link).to(
        eq('https://library.duke.edu/using/policies/quiet-study')
      )
    end

    it 'has an description' do
      expect(results.first.description).to(
        include('<em>Study</em> <em>Zone</em> <em>Policies</em> | ' \
                "Duke University Libraries\nSkip to main content\n" \
                "My Accounts\nAsk a Librarian")
      )
    end
  end
end
