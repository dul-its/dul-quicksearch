# frozen_string_literal: true

describe QuickSearch::DulSummonSearcher do
  let(:searcher) do
    described_class.new(HTTPClient.new, 'quarks', 1)
  end

  it 'has a loaded link' do
    expect(searcher.loaded_link).to(
      eq('https://duke.summon.serialssolutions.com/search?' \
         'fvf=ContentType%2CJournal+Article%2Cf%7CContentType%2CMagazine+Article%2Cf&keep_r=true&l=en&q=quarks')
    )
  end

  describe '#results' do
    describe 'article result' do
      before do
        VCR.use_cassette('searchers/dul_summon_searcher/quarks') do
          searcher.search
        end
      end

      let(:results) do
        VCR.use_cassette('searchers/dul_summon_searcher/quarks_results', match_requests_on: [:host]) do
          searcher.results
        end
      end

      it 'has a total' do
        expect(searcher.total).to be_truthy
      end

      it 'has a title' do
        expect(results.first.title).to(
          eq('Phase structure of completely asymptotically free SU( Nc ) models with <h>quarks</h> ' \
             'and scalar <h>quarks</h>')
        )
      end

      it 'has a link' do
        expect(results.first.link).to include('http://duke.summon.serialssolutions.com/2.0.0/link/0/')
      end

      it 'has a true fulltext value' do
        expect(results.first.fulltext).to(
          be(true)
        )
      end

      it 'has a full text link' do
        expect(results.first.fulltext_link).to include('http://duke.summon.serialssolutions.com/2.0.0/link/0/')
      end

      # rubocop:disable RSpec/ExampleLength
      it 'has linked authors' do
        expect(results.first.author).to(
          eq("by <a href='https://duke.summon.serialssolutions.com/search?s.dym=false&s.q=Author%3A%22Mann, R. B%22'>" \
             "Mann, R. B</a>; <a href='https://duke.summon.serialssolutions.com/search?s.dym=false&s.q=Author%3A%22St" \
             "eele, T. G%22'>Steele, T. G</a>; <a href='https://duke.summon.serialssolutions.com/search?s.dym=false&s" \
             ".q=Author%3A%22Langæble, K%22'>Langæble, K</a>; <a href='https://duke.summon.serialssolutions.com/searc" \
             "h?s.dym=false&s.q=Author%3A%22Wang, Z. W%22'>Wang, Z. W</a>; <a href='https://duke.summon.serialssoluti" \
             "ons.com/search?s.dym=false&s.q=Author%3A%22Sannino, F%22'>Sannino, F</a>; <a href='https://duke.summon." \
             "serialssolutions.com/search?s.dym=false&s.q=Author%3A%22Hansen, F. F%22'>Hansen, F. F</a> and <a href='" \
             "https://duke.summon.serialssolutions.com/search?s.dym=false&s.q=Author%3A%22Janowski, T%22'>Janowski, T" \
             '</a>')
        )
      end
      # rubocop:enable RSpec/ExampleLength

      it 'has a publisher' do
        expect(results.first.publisher).to(
          eq('Physical Review. D')
        )
      end

      it 'has a volume' do
        expect(results.first.volume).to eq('97')
      end

      it 'has an issue' do
        expect(results.first.issue).to(
          eq('6')
        )
      end

      it 'has a format' do
        expect(results.first.format).to(
          eq('Journal Article')
        )
      end
    end

    describe 'truncated author article result' do
      let(:searcher) do
        described_class.new(HTTPClient.new, 'Search for pair production of heavy vector-like quarks decaying', 1)
      end
      let(:results) do
        VCR.use_cassette('searchers/dul_summon_searcher/decaying_quarks_results', match_requests_on: [:host]) do
          searcher.results
        end
      end

      before do
        VCR.use_cassette('searchers/dul_summon_searcher/decaying_quarks') do
          searcher.search
        end
      end

      it 'has a title' do
        expect(results.first.title).to(
          eq('<h>Search for pair production of heavy vector</h>-<h>like quarks decaying</h> into high-p T W bosons ' \
             'and top <h>quarks</h> in the lepton-plus-jets final state in pp collisions at s = 13 TeV with the ATL' \
             'AS detector')
        )
      end

      it 'has pages' do
        expect(results.first.pages).to(
          eq('1&nbsp;-&nbsp;41')
        )
      end

      # rubocop:disable RSpec/ExampleLength
      it 'has linked authors' do
        expect(results.first.author).to(
          eq("by <a href='https://duke.summon.serialssolutions.com/search?s.dym=false&s.q=Author%3A%22G Gustavino%22" \
             "'>G Gustavino</a>; <a href='https://duke.summon.serialssolutions.com/search?s.dym=false&s.q=Author%3A%" \
             "22D Muenstermann%22'>D Muenstermann</a>; <a href='https://duke.summon.serialssolutions.com/search?s.dy" \
             "m=false&s.q=Author%3A%22R Soualah%22'>R Soualah</a>; <a href='https://duke.summon.serialssolutions.com" \
             "/search?s.dym=false&s.q=Author%3A%22A Marzin%22'>A Marzin</a>; <a href='https://duke.summon.serialssol" \
             "utions.com/search?s.dym=false&s.q=Author%3A%22N Ozturk%22'>N Ozturk</a>; <a href='https://duke.summon." \
             "serialssolutions.com/search?s.dym=false&s.q=Author%3A%22R Leitner%22'>R Leitner</a>; <a href='https://" \
             "duke.summon.serialssolutions.com/search?s.dym=false&s.q=Author%3A%22J Wagner-Kuhr%22'>J Wagner-Kuhr</a" \
             ">; <a href='https://duke.summon.serialssolutions.com/search?s.dym=false&s.q=Author%3A%22A Sansoni%22'>" \
             "A Sansoni</a>; <a href='https://duke.summon.serialssolutions.com/search?s.dym=false&s.q=Author%3A%22P " \
             "Vankov%22'>P Vankov</a>; <a href='https://duke.summon.serialssolutions.com/search?s.dym=false&s.q=Auth" \
             "or%3A%22C A Gottardo%22'>C A Gottardo</a>;  et al.")
        )
      end
      # rubocop:enable RSpec/ExampleLength
    end

    describe 'libkey full text result' do
      let(:searcher) do
        described_class.new(HTTPClient.new, 'doris duke', 1)
      end
      let(:results) do
        VCR.use_cassette('searchers/dul_summon_searcher/doris_duke_results', match_requests_on: [:host]) do
          searcher.results
        end
      end

      before do
        VCR.use_cassette('searchers/dul_summon_searcher/doris_duke') do
          searcher.search
        end
      end

      it 'has a doi' do
        expect(results.first.doi).to(
          eq('10.1097/ACM.0b013e3182a7a38e')
        )
      end

      it 'has a libkey link' do
        expect(results.first.libkey).to(
          eq('https://libkey.io/libraries/229/articles/47977498/full-text-file?utm_source=api_871')
        )
      end
    end

    describe 'both no doi result and no libkey result' do
      let(:searcher) do
        described_class.new(HTTPClient.new, 'chess', 1)
      end
      let(:results) do
        VCR.use_cassette('searchers/dul_summon_searcher/chess_results', match_requests_on: [:host]) do
          searcher.results
        end
      end

      before do
        VCR.use_cassette('searchers/dul_summon_searcher/chess_duke') do
          searcher.search
        end
      end

      it 'does not have a doi' do
        expect(results.first.doi).to(
          be_nil
        )
      end

      it 'does not have a libkey link' do
        expect(results.first.libkey).to(
          be_nil
        )
      end
    end

    describe 'doi result, but no libkey result' do
      let(:searcher) do
        described_class.new(HTTPClient.new, 'Transparency is key to ethical vaccine research-Response', 1)
      end
      let(:results) do
        VCR.use_cassette('searchers/dul_summon_searcher/vaccine_research_results', match_requests_on: [:host]) do
          searcher.results
        end
      end

      before do
        VCR.use_cassette('searchers/dul_summon_searcher/vaccine_research') do
          searcher.search
        end
      end

      it 'does have a doi' do
        expect(results.first.doi).to(
          eq('10.1126/science.abf7809')
        )
      end

      it 'does not have a libkey link' do
        expect(results.first.libkey).to(
          be_nil
        )
      end
    end

    describe 'no "full text online" result' do
      let(:searcher) do
        described_class.new(HTTPClient.new, 'Researching the fourteenth edition of The Chicago Manual of Style', 1)
      end
      let(:results) do
        VCR.use_cassette('searchers/dul_summon_searcher/chicago_results', match_requests_on: [:host]) do
          searcher.results
        end
      end

      before do
        VCR.use_cassette('searchers/dul_summon_searcher/chicago') do
          searcher.search
        end
      end

      it 'does not have a fulltext link' do
        expect(results.first.fulltext_link).to(
          be_nil
        )
      end
    end
  end
end
