# frozen_string_literal: true

describe QuickSearch::CatalogArchiveSearcher do
  let(:searcher) do
    described_class.new(HTTPClient.new, 'north carolina', 1)
  end
  let(:param_expectation) do
    { q: 'north carolina',
      rows: 1,
      search_field: 'all_fields',
      'f[resource_type_f][]' => 'Archival and manuscript material' }
  end

  it 'has parameters' do
    expect(searcher.parameters).to eq(param_expectation)
  end

  it 'has a loaded link' do
    expect(searcher.loaded_link).to(
      eq('https://find.library.duke.edu/?' \
         'f[resource_type_f][]=Archival+and+manuscript+material&' \
         'search_field=all_fields&q=north+carolina')
    )
  end
end
