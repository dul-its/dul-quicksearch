# frozen_string_literal: true

describe QuickSearch::DulArclightSearcher do
  let(:searcher) do
    described_class.new(HTTPClient.new, 'duke basketball', 1)
  end

  it 'has HTTPClient' do
    expect(searcher.http).to be_a(HTTPClient)
  end

  it 'has a query' do
    expect(searcher.q).to eq('duke basketball')
  end

  it 'has per page' do
    expect(searcher.per_page).to eq(1)
  end

  it 'has a base url' do
    expect(searcher.base_url).to(
      eq('https://archives.lib.duke.edu/catalog.json?')
    )
  end

  it 'has parameters' do
    expect(searcher.parameters).to(
      eq({ q: 'duke basketball', rows: 1, search_field: 'all_fields' })
    )
  end

  it 'has a loaded link' do
    expect(searcher.loaded_link).to(
      eq('https://archives.lib.duke.edu/catalog?' \
         'group=true&search_field=all_fields&q=duke+basketball')
    )
  end

  describe '#results' do
    describe 'collection guide result with parents' do
      before do
        VCR.use_cassette('searchers/dul_arclight_searcher/duke_basketball') do
          searcher.search
        end
      end

      let(:results) do
        searcher.results
      end

      it 'has a total' do
        expect(searcher.total).to be_truthy
      end

      it 'has a title' do
        expect(results.first.title).to eq('Duke Basketball, undated')
      end

      it 'has a link' do
        expect(results.first.link).to(
          eq('https://archives.lib.duke.edu/catalog/uamensbasketballposters_aspace_ref51_ur0')
        )
      end

      it 'has a description' do
        expect(results.first.description).to eq('Illustration of Blue Devil mascot dunking ball.')
      end

      # rubocop:disable RSpec/ExampleLength
      it 'has linked parents' do
        expect(results.first.linked_parent_labels).to(
          eq([["Men's Basketball poster collection, 1959-2013",
               'https://archives.lib.duke.edu/catalog/uamensbasketballposters'],
              ["Men's Basketball Poster Collection",
               'https://archives.lib.duke.edu/catalog/uamensbasketballposters_aspace_ref154_iyc']])
        )
      end
      # rubocop:enable RSpec/ExampleLength
    end

    describe 'collection guide result with icons' do
      let(:searcher) do
        described_class.new(HTTPClient.new, 'Series of passport photos of Doris Duke', 1)
      end
      let(:results) do
        searcher.results
      end

      before do
        VCR.use_cassette('searchers/dul_arclight_searcher/doris_duke') do
          searcher.search
        end
      end

      it 'has a total' do
        expect(searcher.total).to be_truthy
      end

      it 'has a title' do
        expect(results.first.title).to eq('Series of passport photos of Doris Duke, 1930-1990')
      end

      it 'has a link' do
        expect(results.first.link).to(
          eq('https://archives.lib.duke.edu/catalog/dorisdukephotos_aspace_ref18_5iq')
        )
      end

      it 'has icons' do
        expect(results.first.icon).to(
          include("<div class='icon-wrapper'>")
        )
      end
    end
  end
end
