# frozen_string_literal: true

RSpec.describe QuickSearch::PagesController do
  routes { QuickSearch::Engine.routes }
  describe 'GET about' do
    it "raises 'Not Found'" do
      expect { get :about }.to raise_error(ActionController::RoutingError)
    end
  end
end
