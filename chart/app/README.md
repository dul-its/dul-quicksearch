# Initial Setup (after creating Helm Chart folder)

## Create Secrets

```sh
$ kubectl create secret generic db-secret \
--from-literal=postgres-password=<strongpassword> \
--from-literal=password=<strongpassword>
```

## Create DNS Records
Visit https://aka.oit.duke.edu to create CNAME DNS records for the 
preferred domain.  
  

