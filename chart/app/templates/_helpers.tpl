{{/*
Expand the name of the chart.
*/}}
{{- define "app.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "app.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "app.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "app.labels" -}}
helm.sh/chart: {{ include "app.chart" . }}
{{ include "app.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "app.selectorLabels" -}}
app.kubernetes.io/name: {{ include "app.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "app.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "app.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the name of the database service 
*/}}
{{- define "app.dbServiceFullname" -}}
{{- if .ReleaseName -}}
{{ printf "%s-postgresql" .ReleaseName }}
{{- else -}}
{{ printf "%s-postgresql" .Release.Name }}
{{- end -}}
{{- end }}

{{/*
*/}}
{{- define "app.postgresSecrets" -}}
db-secrets
{{- end }}

{{/*
Create the name of the app secrets
*/}}
{{- define "app.appSecrets" -}}
app-secrets
{{- end }}

{{/*
Create the name of the app secrets
*/}}
{{- define "app.apiSecrets" -}}
api-secrets
{{- end }}

{{- define "app.databaseUser" }}
qs
{{- end -}}

{{- define "app.databaseName" }}
qs
{{- end }}

{{/*
Get value from a secret
TIP SOURCE: 
https://stackoverflow.com/questions/50452665/import-data-to-config-map-from-kubernetes-secret
*/}}
{{- define "getValueFromSecret" -}}
{{- $len := (default 16 .Length) | int -}}
{{- $obj := (lookup "v1" "Secret" .Namespace .Name).data -}}
{{- if $obj }}
{{- index $obj .Key | b64dec -}}
{{- else -}}
{{- randAlphaNum $len -}}
{{- end -}}
{{- end -}}

{{- define "app.databaseUrl" -}}
{{- $dbUser := (include "app.databaseUser" .Context ) -}}
{{- $dbName := (include "app.databaseName" .Context ) -}}
{{ $dbHost := (include "app.dbServiceFullname" (dict "ReleaseName" .Context.Release.Name)) -}}
{{ $dbPass := (include "getValueFromSecret" (dict "Namespace" .Context.Release.Namespace "Name" "db-secrets" "Key" "password")) -}}
{{ printf "postgres://%s:%s@%s/%s" (trim $dbUser) $dbPass $dbHost (trim $dbName) }}
{{- end }}
