# frozen_string_literal: true

require_dependency QuickSearch::Engine.config
                                      .root
                                      .join('app',
                                            'helpers',
                                            'quick_search',
                                            'application_helper.rb').to_s

module QuickSearch
  module ApplicationHelper
    def application_name
      if action_name == 'single_searcher'
        I18n.t "#{params[:searcher_name]}_search.title_name"
      else
        I18n.t 'defaults_search.title_name'
      end
    end
  end
end
