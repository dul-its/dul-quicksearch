# frozen_string_literal: true

module ApplicationHelper
  def autosuggest_state
    if action_name == 'single_searcher'
      'false'
    else
      'true'
    end
  end

  def autocomplete_state
    if action_name == 'single_searcher'
      'on'
    else
      'off'
    end
  end
end
