// Enables Bootstrap tooltips
// https://getbootstrap.com/docs/4.5/components/tooltips/#example-enable-tooltips-everywhere

document.addEventListener("turbolinks:load", function() {

  /* borrowed from dul_arclight: */

  var popOverSettings = {
    placement: 'auto',
    container: 'body',
    boundary: 'viewport',
    html: true,
    trigger: 'hover',
    selector: '[data-toggle="popover"]',
    content: function () {
      return $(this).data('content');
    }
  };

  $('body').popover(popOverSettings);

  $('body').tooltip({
    selector: '[data-toggle="tooltip"]'
  });

});
