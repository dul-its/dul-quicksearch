// Sets focus on main search field via hack from:
// https://github.com/corejavascript/typeahead.js/issues/156

document.addEventListener("turbolinks:load", function() {

  var urlParams = new URLSearchParams(window.location.search);
  var q = urlParams.get('q');

  if(!q) {
    setTimeout(function(){
      $('#main-search-form .tt-input').focus();
    }, 250);
  }

});
