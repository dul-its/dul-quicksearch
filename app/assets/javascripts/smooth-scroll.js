// Enables smooth scrolling for anchor links

document.addEventListener("turbolinks:load", function() {
  
  // Add smooth scrolling to all links
  $('#result-types li a').on('click', function(event) {
    if (this.hash !== '') {
      event.preventDefault();
      var hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top + -20
      }, 800, function(){
        window.location.hash = hash;
      });
    }
  });

});
