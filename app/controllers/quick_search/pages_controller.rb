# frozen_string_literal: true

require_dependency QuickSearch::Engine.config
                                      .root
                                      .join('app',
                                            'controllers',
                                            'quick_search',
                                            'pages_controller.rb').to_s

module QuickSearch
  class PagesController
    def about
      raise ActionController::RoutingError, 'Not Found'
    end
  end
end
