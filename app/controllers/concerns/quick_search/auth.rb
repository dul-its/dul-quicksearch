# frozen_string_literal: true

# Override QuickSearch core auth module to use ENV vars rather than public YML
# See:
# https://github.com/NCSU-Libraries/quick_search/blob/master/app/controllers/concerns/quick_search/auth.rb

require "digest/md5"

module QuickSearch::Auth
  extend ActiveSupport::Concern

  SALT01 = SecureRandom.hex(512)
  SALT02 = SecureRandom.hex(512)
  REALM = "Please enter the username and password to access this page"

  # DUL CUSTOMIZATION: these two variables
  USERS = {"#{DulQuicksearch.appstats_user}" => Digest::MD5.hexdigest(["#{DulQuicksearch.appstats_user}",REALM,"#{DulQuicksearch.appstats_password}"].join(":"))}
  SALTY = SALT01 + Digest::MD5.hexdigest(["#{DulQuicksearch.appstats_user}",REALM,"#{DulQuicksearch.appstats_password}"].join(":")) + SALT02

  def auth
    authenticate_or_request_with_http_digest(REALM) do |username|
      unless SALT01.blank? or SALT02.blank? or USERS[username].blank?
        if SALT01 + USERS[username] + SALT02 == SALTY
          USERS[username]
        end
      end
    end
  end
end
