# frozen_string_literal: true

# Override QuickSearch core QueryFilter module to be less agressive
# (don't strip dashes (-).)
# See:
# https://github.com/NCSU-Libraries/quick_search/blob/master/app/controllers/concerns/quick_search/query_filter.rb

module QuickSearch
  module QueryFilter
    extend ActiveSupport::Concern

    include ActionView::Helpers::TextHelper

    private

    def filter_query(query)
      query = query.sub(/ -$/, '') if query.match(/ -$/)
      query.gsub!('*', ' ')
      query.gsub!('!', ' ')
      # DUL Customization: remove dash filter
      # query.gsub!('-', ' ')
      query.gsub!('\\', '')
      # query.gsub!('"', '')
      query.strip!
      query.squish!
      query.downcase!

      truncate(query, length: 100, separator: ' ', omission: '', escape: false)
    end
  end
end
