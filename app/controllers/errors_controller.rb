# frozen_string_literal: true

class ErrorsController < ActionController::Base
  def forbidden
    render status: 403
  end

  def not_found
    render status: 404
  end

  def rejected
    render status: 422
  end

  def internal_server
    render status: 500
  end
end
