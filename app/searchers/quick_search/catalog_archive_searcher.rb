# frozen_string_literal: true

module QuickSearch
  class CatalogArchiveSearcher < CatalogSearcher
    def parameters
      {
        search_field: 'all_fields',
        q: http_request_queries['not_escaped'],
        rows: @per_page,
        'f[resource_type_f][]' => 'Archival and manuscript material'
      }
    end

    def loaded_link
      'https://find.library.duke.edu/?' \
        'f[resource_type_f][]=Archival+and+manuscript+material&' \
        'search_field=all_fields&q=' \
        "#{http_request_queries['uri_escaped']}"
    end
  end
end
