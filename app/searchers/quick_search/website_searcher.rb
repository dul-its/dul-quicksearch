# frozen_string_literal: true

require 'ostruct'

module QuickSearch
  class WebsiteSearcher < QuickSearch::Searcher
    def search
      url = [base_url, parameters.to_query].join
      resp = @http.get(url)
      @response = JSON.parse(resp.body)
    end

    # rubocop:disable Metrics/MethodLength
    # rubocop:disable Style/OpenStructUse
    def results
      if results_list
        results_list
      else
        @results_list = []
        @response['response']['docs'].each do |value|
          result = OpenStruct.new
          result.title = title(value)
          result.link = link(value)
          result.description = description(value)
          @results_list << result
        end

        @results_list
      end
    end
    # rubocop:enable Metrics/MethodLength
    # rubocop:enable Style/OpenStructUse

    def base_url
      DulQuicksearch.website_searcher_base_url
    end

    # rubocop:disable Metrics/MethodLength
    def parameters
      {
        q: http_request_queries['not_escaped'],
        rows: @per_page,
        start: @offset,
        bq: 'subcollection:main^100',
        qf: 'title^250 slug^50 anchor^50 h1^50 h2^25 h3^12 content',
        pf: 'title^500 slug^100 anchor^100 h1^100 h2^50 h3^24 content^5',
        hl: 'true',
        'hl.fl' => 'content',
        defType: 'edismax',
        echoParams: 'explicit',
        mm: '3<-1 5<-2 6<90%',
        'q.alt' => '*:*'
      }
    end
    # rubocop:enable Metrics/MethodLength

    def total
      @response.fetch('response', {}).fetch('numFound', 0)
    end

    def loaded_link
      "/searcher/website?q=#{http_request_queries['uri_escaped']}"
    end

    def paging
      Kaminari.paginate_array(results, total_count: total.to_i)
              .page(@page)
              .per(@per_page)
    end

    private

    def title(value)
      [value.fetch('title', nil) || value.fetch('id', 'Title Missing')].flatten.first
    end

    def link(value)
      [value.fetch('url', nil)].flatten.first
    end

    def description(value)
      id = [value.fetch('id', nil)].flatten.first
      @response.fetch('highlighting', {})
               .fetch(id, {})
               .fetch('content', [])
               .first
    end
  end
end
