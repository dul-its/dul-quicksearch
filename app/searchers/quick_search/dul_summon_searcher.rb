# frozen_string_literal: false

# Extends quick search summon searcher core file
# https://github.com/NCSU-Libraries/quick_search-summon_searcher/blob/master/
# app/searchers/quick_search/summon_searcher.rb

require 'ostruct'
require 'json'

module QuickSearch
  class DulSummonSearcher < SummonSearcher
    def search
      url = api_base_url + api_parameters.to_query
      raw_response = @http.get(url, {}, headers)

      begin
        @response = JSON.parse(raw_response.body)
      rescue JSON::ParserError
        @response = {}
      end
    end

    # rubocop:disable Metrics/AbcSize
    # rubocop:disable Metrics/MethodLength
    # rubocop:disable Style/OpenStructUse
    def results
      return results_list if results_list

      pages = @response['documents'] || []
      @results_list = []

      pages.each do |value|
        result = OpenStruct.new
        result.title = title(value)
        result.link = link(value)
        result.format = format(value)
        result.description = description(value)
        result.citation = true
        result.author = author(value)
        result.date = date(value)
        result.publisher = publisher(value)
        result.volume = volume(value)
        result.issue = issue(value)
        result.pages = pages(value)
        result.fulltext = fulltext(value)
        result.doi = doi(value)
        result.fulltext_link = fulltext_link(value)
        result.libkey = libkey(value)

        result.thumbnail = thumbnail(value) if QuickSearch::Engine::SUMMON_CONFIG['show_thumbnails']

        @results_list << result
      end

      @results_list
    end
    # rubocop:enable Metrics/AbcSize
    # rubocop:enable Metrics/MethodLength
    # rubocop:enable Style/OpenStructUse

    private

    def headers
      Summon::Transport::Headers.new(
        url: api_base_url,
        access_id: DulQuicksearch.summon_access_id,
        client_key: DulQuicksearch.summon_client_base,
        secret_key: DulQuicksearch.summon_secret_key,
        accept: 'json',
        params: api_parameters
      )
    end

    # rubocop:disable Metrics/MethodLength
    def api_parameters
      api_parameters = {
        's.include.ft.matches' => 'false',
        's.cmd' => commands.join(' '),
        's.light' => 'true',
        's.pn' => '1',
        's.ps' => @per_page,
        's.q' => http_request_queries['not_escaped'],
        's.rec.topic.max' => '1'
      }

      api_parameters['s.role'] = 'authenticated' if @on_campus

      api_parameters
    end
    # rubocop:enable Metrics/MethodLength

    def description(value)
      # hide description
    end

    def author(value)
      return unless value['Author']

      linked_authors = 'by '
      value['Author'].each_with_index do |author, index|
        linked_authors << "<a href='#{loaded_link_base_url}s.dym=false&s.q=Author%3A%22#{author}%22'>#{author}</a>"
        linked_authors << '; ' if author != value['Author'].last
        return "#{linked_authors} et al." if index == max_authors
      end
      linked_authors.reverse.sub(';', 'dna ').reverse
    end

    def max_authors
      9
    end

    def fulltext_link(value)
      return unless value['hasFullText']

      value['link']
    end

    def libkey(value)
      return unless doi(value)

      id = DulQuicksearch.libkey_id
      token = DulQuicksearch.libkey_api_token
      uri = URI.parse("https://public-api.thirdiron.com/public/v1/libraries/#{id}/articles/doi/#{@doi}?access_token=#{token}")
      response = Net::HTTP.get_response(uri)
      return unless valid_json?(response.body)

      libkey_json = JSON.parse(response.body)
      return unless libkey_json.dig('data', 'fullTextFile').present?

      libkey_json['data']['fullTextFile']
    end

    def valid_json?(response)
      JSON.parse(response)
      true
    rescue JSON::ParserError
      false
    end

    def doi(value)
      return unless value['DOI']

      @doi = value['DOI'][0]
    end
  end
end
