# frozen_string_literal: true

# API query example
# https://find.library.duke.edu/catalog.json?search_field=all_fields&q=how%20milton%20works&rows=3

require 'ostruct'

module QuickSearch
  # rubocop:disable Metrics/ClassLength
  class CatalogSearcher < QuickSearch::Searcher
    def search
      url = [base_url, parameters.to_query].join
      resp = @http.get(url)
      @response = JSON.parse(resp.body)
    end

    # rubocop:disable Metrics/AbcSize
    # rubocop:disable Metrics/MethodLength
    # rubocop:disable Style/OpenStructUse
    def results
      if results_list
        results_list
      else
        @results_list = []
        @response['data'].each do |value|
          result = OpenStruct.new
          result.id = value.fetch('id', 'ID Missing')
          result.title = title(value)
          result.format = format(value)
          result.link = build_link(result.id)
          result.author = author(value)
          result.availability = availability(value) if library_location(value)
          result.location = library_location(value)
          result.fulltext = fulltext(value)
          result.fulltext_link = view_online_link(value)
          result.thumbnail = thumbnail(value)
          @results_list << result
        end

        @results_list
      end
    end
    # rubocop:enable Metrics/AbcSize
    # rubocop:enable Metrics/MethodLength
    # rubocop:enable Style/OpenStructUse

    def api_endpoint
      # 'https://find.library.duke.edu/'
      DulQuicksearch.catalog_searcher_api_endpoint
    end

    def base_url
      "#{api_endpoint}catalog.json?"
    end

    def parameters
      {
        search_field: 'all_fields',
        q: http_request_queries['not_escaped'],
        rows: @per_page
      }
    end

    def total
      @response['meta']['pages']['total_count']
    end

    def loaded_link
      "#{api_endpoint}?search_field=all_fields&q=#{http_request_queries['uri_escaped']}"
    end

    private

    # ##########################
    # Metadata
    # ##########################

    def title(value)
      return 'Title Missing' unless value['attributes']['title_main']

      value['attributes']['title_main']['attributes']['value']
    end

    def format(value)
      return unless value['attributes']['resource_type_a']

      value['attributes']['resource_type_a']['attributes']['value']
    end

    def build_link(id)
      "#{api_endpoint}catalog/#{id}"
    end

    # temporarily redefine availability method while Lilly renovation is ongoing
    # def availability(value)
    #   return 'Not Available' unless value['attributes']['available_a']

    #   value['attributes']['available_a']['attributes']['value']
    # end

    def availability(value)
      availability_value = if value['attributes']['mms_id_a'] # alma records
                             alma_availability(value)
                           else
                             aleph_availability(value) # catch stale Aleph records for now
                           end

      availability_value.to_s
    end

    def alma_availability(value)
      return 'Not Available' unless value['attributes']['holdings_a']

      raw_result = CGI.unescapeHTML(value['attributes']['holdings_a']['attributes']['value'])

      if raw_result.include?('}<br />{')
        multiple_location_lilly_check(raw_result, value)
      else
        single_location_lilly_check(raw_result, value)
      end
    end

    def aleph_availability(value)
      return 'Not Available' unless value['attributes']['available_a']

      value['attributes']['available_a']['attributes']['value']
    end

    # temp helper for Lilly renovation
    # example: {"loc_b":"PERKN","loc_n":"PK8","status":"Available","call_no":"PS1306 .A1 1991a"}
    def single_location_lilly_check(raw_result, _value)
      result = JSON.parse(raw_result)
      if result['loc_b'] == 'LILLY'
        'Not Available'
      else
        result['status']
      end
    end

    # temp helper for Lilly renovation
    def multiple_location_lilly_check(raw_result, _value)
      parsed_i = split_raw_items(raw_result).map { |i| JSON.parse(i) }

      # Filter out items that are located at 'LILLY'
      non_lilly_items = parsed_i.reject { |item| item['loc_b'].downcase == 'lilly' }

      # Check if any of the non-LILLY items are available
      if non_lilly_items.any? { |item| item['status'].downcase.start_with?('available') }
        'Available'
      elsif non_lilly_items.any? { |item| item['status'].downcase == 'check holdings' }
        'Check Holdings'
      else
        'Not Available'
      end
    end

    def author(value)
      return unless value['attributes']['statement_of_responsibility_a']

      value['attributes']['statement_of_responsibility_a']['attributes']['value']
    end

    # ##########################
    # View Online Linking
    # ##########################

    def view_online_link(value)
      record_urls(value)
    end

    def fulltext(value)
      record_urls(value).present?
    end

    # rubocop:disable Lint/UnreachableLoop
    def split_raw_result(raw_result)
      raw_result.split('<br />').map do |r|
        result = JSON.parse(r)
        break unless result['type'] == 'fulltext'

        return result['href']
      end
    end
    # rubocop:enable Lint/UnreachableLoop

    def record_urls(value)
      return unless value['attributes']['url_a']

      raw_result = CGI.unescapeHTML(value['attributes']['url_a']['attributes']['value'])
      if raw_result.include?('}<br />{')
        return unless split_raw_result(raw_result).present?

        split_raw_result(raw_result)
      else
        result = JSON.parse(raw_result)
        result['href'] if result['type'] == 'fulltext'
      end
    end

    # ##########################
    # Item Location
    # ##########################

    def library_location(value)
      return unless value['attributes']['items_a']

      raw_result = CGI.unescapeHTML(value['attributes']['items_a']['attributes']['value'])
      if raw_result.include?('}<br />{')
        multiple_items_display(raw_result)
      else
        single_item_display(raw_result)
      end
    end

    def split_raw_items(raw_result)
      items = []
      raw_result.split('<br />').map do |r|
        items << r
      end
      items
    end

    def multiple_items_display(raw_result)
      parsed_i = split_raw_items(raw_result).map { |i| JSON.parse(i) }
      decoded_i = parsed_i.map { |i| location_codes['loc_b'][i.fetch('loc_b', '')].to_s }.select(&:present?).uniq
      decoded_i.count > 3 ? "#{decoded_i[0..2].join(', ')}..." : decoded_i.join(', ')
    end

    def single_item_display(raw_result)
      result = JSON.parse(raw_result)
      call_no_copy_no = [result['call_no'], result['copy_no']].join(' ')
      "#{location_codes['loc_b'][result['loc_b']]}, " \
      "#{call_no_copy_no.gsub(' ', '&nbsp;')}".html_safe
    end

    def location_codes
      @location_codes ||= DulQuicksearch.location_codes
    end

    # ##########################
    # Syndetics Thumbnail
    # ##########################

    def thumbnail(value)
      return unless thumbnail_url(value)

      image_size = FastImage.size(thumbnail_url(value), timeout: 0.1)
      thumbnail_url(value) if !image_size.nil? && image_size.first > 1
    end

    def thumbnail_url(value)
      params = syndetics_params(value)
      return unless params

      [syndetics_base_url, params.to_query.gsub('%5B%5D', ''), '/MC.gif'].join
    end

    def syndetics_base_url
      'https://syndetics.com/index.php?'
    end

    def split_raw_params(raw_params)
      param_numbers = []
      raw_params.split('<br />').map do |r|
        param_numbers << r.gsub('ISBN: ', '').gsub('OCLC: ', '').gsub('UPC: ', '')
      end
      param_numbers
    end

    # ISBN
    def isbn_first_value(value)
      primary_isbn_a = value['attributes']['primary_isbn_a']
      isbn_number_a = value['attributes']['isbn_number_a']
      qualifying_isbn = value['attributes']['isbn_with_qualifying_info_a']

      return unless primary_isbn_a.present? || isbn_number_a.present? || qualifying_isbn.present?

      return primary_isbn_a(value) if primary_isbn_a.present?

      return isbn_number_a(value) if isbn_number_a.present?

      isbn_with_qualifying_info_a(value) if qualifying_isbn.present?
    end

    def primary_isbn_a(value)
      return unless value['attributes']['primary_isbn_a'].present?

      the_isbn = value['attributes']['primary_isbn_a']['attributes']['value']
      raw_isbn = CGI.unescapeHTML(the_isbn)
      return the_isbn unless raw_isbn.include?('<br />')

      split_raw_params(raw_isbn)[0]
    end

    def isbn_number_a(value)
      return unless value['attributes']['isbn_number_a'].present?

      value['attributes']['isbn_number_a']['attributes']['value']
    end

    def isbn_with_qualifying_value(value)
      the_isbn = value['attributes']['isbn_with_qualifying_info_a']['attributes']['value']
      raw_isbn = CGI.unescapeHTML(the_isbn)

      return if raw_isbn == '<br /><br /><br />'

      return the_isbn unless raw_isbn.include?('<br />')

      split_raw_params(raw_isbn)[0]
    end

    # OCLC
    def oclc_first_value(value)
      return unless value['attributes']['primary_oclc_a'] || value['attributes']['oclc_number']

      return primary_oclc_a(value) if value['attributes']['primary_oclc_a'].present?

      oclc_number(value) if value['attributes']['oclc_number'].present?
    end

    def primary_oclc_a(value)
      value['attributes']['primary_oclc_a']['attributes']['value']
    end

    def oclc_number(value)
      value['attributes']['oclc_number']['attributes']['value']
    end

    # UPC
    def upc_first_value(value)
      return unless value['attributes']['primary_upc_a'] || value['attributes']['upc_a']

      return primary_upc_a(value) if value['attributes']['primary_upc_a'].present?

      upc_a(value) if value['attributes']['upc_a'].present?
    end

    def primary_upc_a(value)
      value['attributes']['primary_upc_a']['attributes']['value']
    end

    def upc_a(value)
      the_upc = value['attributes']['upc_a']['attributes']['value']
      raw_upc = CGI.unescapeHTML(the_upc)

      return the_upc unless raw_upc.include?('<br />')

      split_raw_params(raw_upc)[0]
    end

    # Params
    def syndetics_id_params(value)
      {
        isbn: [isbn_first_value(value)].compact,
        oclc: [oclc_first_value(value)].compact,
        upc: [upc_first_value(value)].compact
      }
    end

    def syndetics_params(value)
      params = syndetics_id_params(value)
      %i[isbn oclc upc].each do |k|
        if params.fetch(k, []).empty?
          params.delete(k)
        else
          params[k][0]
          break
        end
      end
      params.empty? ? false : params.update(client: 'trlnet')
    end
  end
  # rubocop:enable Metrics/ClassLength
end
