# frozen_string_literal: true

require 'ostruct'

module QuickSearch
  # rubocop:disable Metrics/ClassLength
  class DulArclightSearcher < QuickSearch::Searcher
    def search
      url = [base_url, parameters.to_query].join
      resp = @http.get(url)
      @response = JSON.parse(resp.body)
      url_grouped = [base_url, parameters_grouped.to_query].join
      resp_grouped = @http.get(url_grouped)
      @response_grouped = JSON.parse(resp_grouped.body)
    end

    # rubocop:disable Metrics/AbcSize
    # rubocop:disable Metrics/MethodLength
    # rubocop:disable Style/OpenStructUse
    def results
      if results_list
        results_list
      else
        @results_list = []
        @response['data'].each do |value|
          result = OpenStruct.new
          result.title = title(value)
          result.link = link(value)
          result.description = description(value)
          result.parent_labels_label = parent_labels_label(value)
          result.online_component = online_component(value)
          result.restricted_component = restricted_component(value)
          result.icon = icon(value)
          result.linked_parent_labels = linked_parent_labels(value)
          @results_list << result
        end
        @results_list
      end
    end
    # rubocop:enable Metrics/AbcSize
    # rubocop:enable Metrics/MethodLength
    # rubocop:enable Style/OpenStructUse

    def base_url
      'https://archives.lib.duke.edu/catalog.json?'
    end

    def parameters
      {
        search_field: 'all_fields',
        q: http_request_queries['not_escaped'],
        rows: @per_page
      }
    end

    def parameters_grouped
      {
        group: 'true',
        search_field: 'all_fields',
        q: http_request_queries['not_escaped'],
        rows: @per_page
      }
    end

    def total
      @response_grouped['meta']['pages']['total_count']
    end

    def loaded_link
      # 'https://archives.lib.duke.edu/?utf8=%E2%9C%93&group=true&search_field=all_fields&' \
      #   "q=#{http_request_queries['uri_escaped']}"
      'https://archives.lib.duke.edu/catalog?group=true&search_field=all_fields&q=' \
        "#{http_request_queries['uri_escaped']}"
    end

    private

    def title(value)
      return unless value['attributes']['normalized_title']

      value['attributes']['normalized_title']['attributes']['value']
    end

    def link(value)
      'https://archives.lib.duke.edu/catalog/' \
        "#{value['id']}"
    end

    def description(value)
      return unless value['attributes']['short_description']

      CGI.unescapeHTML(value['attributes']['short_description']['attributes']['value'])
    end

    # ##########################
    # Linking up Parents
    # ##########################

    def linked_parent_labels(value)
      labels = parent_labels(value)
      ids = parent_ids(value)
      return unless labels && ids

      labels.zip(ids)
    end

    def parent_labels_label(value)
      return unless value['attributes']['parent_labels']

      value['attributes']['parent_labels']['attributes']['label']
    end

    def parent_labels(value)
      return unless value['attributes']['parent_labels']

      value['attributes']['parent_labels']['attributes']['value']
    end

    def parent_ids(value)
      return unless value['attributes']['parent_ids']

      root_id = value['attributes']['parent_ids']['attributes']['value'][0]
      value['attributes']['parent_ids']['attributes']['value'].map.with_index do |v, i|
        if i.zero?
          "https://archives.lib.duke.edu/catalog/#{v}"
        else
          "https://archives.lib.duke.edu/catalog/#{root_id}_#{v}"
        end
      end
    end

    # ##########################
    # Adding Icons
    # ##########################

    def online_component(value)
      return unless value['attributes']['online_content?']

      value['attributes']['online_content?']['attributes']['value']
    end

    def restricted_component(value)
      return unless value['attributes']['restricted_component?']

      value['attributes']['restricted_component?']['attributes']['value']
    end

    def online_icon
      I18n.t 'dul_arclight_search.online_icon_html'
    end

    def restricted_icon
      I18n.t 'dul_arclight_search.restricted_icon_html'
    end

    def icon(value)
      return unless online_component(value) == 'true' || restricted_component(value) == 'true'

      "<div class='icon-wrapper'>" \
        "#{online_icon unless online_component(value) == 'false'} " \
        "#{restricted_icon unless restricted_component(value) == 'false'}" \
        '</div>'
    end
  end
  # rubocop:enable Metrics/ClassLength
end
