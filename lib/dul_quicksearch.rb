# frozen_string_literal: true

require 'dul_quicksearch/version'
require 'active_record'
require 'fastimage' # needed for Summon Searcher

module DulQuicksearch
  autoload :Configurable, 'dul_quicksearch/configurable'
  include DulQuicksearch::Configurable
end
