# frozen_string_literal: true

require 'yaml'

module DulQuicksearch
  module Configurable
    extend ActiveSupport::Concern

    LOCATION_CODE_FILE = File.join(Rails.root, 'config', 'location_item_holdings.yml')

    # rubocop:disable Metrics/BlockLength
    included do
      mattr_accessor :location_codes do
        YAML.load_file(LOCATION_CODE_FILE)
      end

      #############################
      # Catalog Searcher
      #############################

      mattr_accessor :catalog_searcher_api_endpoint do
        ENV['CATALOG_SEARCHER_API_ENDPOINT'] ||
          'https://find.library.duke.edu/'
      end

      #############################
      # Summon Searcher
      #############################

      mattr_accessor :summon_access_id do
        ENV['SUMMON_ACCESS_ID'] || ''
      end

      mattr_accessor :summon_secret_key do
        ENV['SUMMON_SECRET_KEY'] || ''
      end

      mattr_accessor :summon_client_base do
        ENV['SUMMON_CLIENT_BASE'] || ''
      end

      #############################
      # Website Searcher
      #############################

      mattr_accessor :website_searcher_base_url do
        ENV['WEBSITE_SEARCHER_BASE_URL'] ||
          'http://nutch-2.lib.duke.edu:8983/solr/nutch/select?'
      end

      #############################
      # Libkey
      #############################

      mattr_accessor :libkey_id do
        ENV['LIBKEY_ID'] || ''
      end

      mattr_accessor :libkey_api_token do
        ENV['LIBKEY_API_TOKEN'] || ''
      end

      #############################
      # Stats Tracking
      #############################

      mattr_accessor :appstats_user do
        ENV['APPSTATS_USER'] || 'stats'
      end

      mattr_accessor :appstats_password do
        ENV['APPSTATS_PASSWORD'] || 'stats'
      end

      mattr_accessor :google_analytics_tracking_id do
        ENV['GOOGLE_ANALYTICS_TRACKING_ID'] || ''
      end

      mattr_accessor :matomo_analytics_tracking_id do
        ENV['MATOMO_ANALYTICS_TRACKING_ID'] || ''
      end

      mattr_accessor :matomo_analytics_host do
        ENV['MATOMO_ANALYTICS_HOST'] || ''
      end
    end
    # rubocop:enable Metrics/BlockLength

    module ClassMethods
      def configure
        yield self
      end
    end
  end
end
