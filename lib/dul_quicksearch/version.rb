# frozen_string_literal: true

# rubocop:disable Naming/ConstantName
module DulQuicksearch
  Version = '1.2.6'
end
# rubocop:enable Naming/ConstantName
