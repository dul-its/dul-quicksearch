# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.2.0'

gem 'bootsnap', '>= 1.1.0', require: false
# toggle this version of dul_bootstrap_theme for local development
# gem 'dul_bootstrap_theme', path: '/app/vendor/dul_bootstrap_theme'
gem 'dul_bootstrap_theme',
    git: 'https://gitlab.oit.duke.edu/dul-its/dul_bootstrap_theme/',
    tag: 'v1.1.4'
gem 'jbuilder', '~> 2.5'
gem 'jquery-ui-rails'
gem 'mysql2', '~> 0.5.6'
gem 'net-imap'
gem 'net-pop'
gem 'net-smtp'
gem 'pg'
gem 'puma', '~> 6.0'
gem 'quick_search-core', git: 'https://gitlab.oit.duke.edu/dul-its/ncsu-quicksearch-fork.git',
                         tag: 'v1.0.0'
gem 'quick_search-summon_searcher', git: 'https://github.com/NCSU-Libraries/quick_search-summon_searcher',
                                    ref: '1b6d8f66edbb3928d50ac88f33380e16030cdfa8'
gem 'rack-attack'
gem 'rails', '~> 7.0'
gem 'sassc-rails'
gem 'turbolinks', '~> 5'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
gem 'uglifier', '>= 1.3.0'

group :development, :test do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'rspec-rails', '~> 6.1'
  gem 'rubocop', '~> 1.6'
  gem 'rubocop-rspec'
  gem 'selenium-webdriver', '>= 4.11'
  gem 'vcr'
  gem 'webmock'
end

group :development do
  gem 'listen', '~> 3.5'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara', '>= 2.15'
end
