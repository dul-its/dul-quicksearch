#!/bin/sh

# https://stackoverflow.com/a/38732187/1935918
set -e

# this location needs to match the 
# WORKDIR /my/app/stuff location 
# from the canonical Dockerfile
if [ -f /usr/src/app/tmp/pids/server.pid ]; then
  rm /usr/src/app/tmp/pids/server.pid
fi

# This entrypoint file doesn't need to be nearly as 
# complex as what we've done with argon-skin, staff-directory, etc.

# To be honest, do we even need this line below?
bundle exec rake db:migrate 2>/dev/null || bundle exec rake db:setup

exec bundle exec "$@"
