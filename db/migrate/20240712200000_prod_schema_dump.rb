class ProdSchemaDump < ActiveRecord::Migration[6.1]

  def change

    # These are extensions that must be enabled in order to support this database
    enable_extension "plpgsql"

    create_table "events", id: :serial, force: :cascade do |t|
      t.string "category"
      t.string "item"
      t.string "query"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string "action"
      t.integer "session_id"
      t.string "created_at_string"
      t.index ["created_at_string"], name: "index_events_on_created_at_string"
      t.index ["session_id"], name: "index_events_on_session_id"
    end

    create_table "searches", id: :serial, force: :cascade do |t|
      t.string "query"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string "page"
      t.integer "session_id"
      t.string "created_at_string"
      t.index ["created_at_string"], name: "index_searches_on_created_at_string"
      t.index ["session_id"], name: "index_searches_on_session_id"
    end

    create_table "sessions", id: :serial, force: :cascade do |t|
      t.string "session_uuid"
      t.datetime "expiry"
      t.boolean "on_campus"
      t.boolean "is_mobile"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.string "created_at_string"
      t.index ["created_at_string"], name: "index_sessions_on_created_at_string"
    end

    add_foreign_key "events", "sessions"
    add_foreign_key "searches", "sessions"
  end

end